function go(){
     //grab my variables
     start = document.getElementById("start").value.trim();
     num = document.getElementById("num").value.trim();
     //check validity
     if (start.length < 1 || num.length < 1 || isNaN(start) || isNaN(num)){     
          alert("Please enter two numbers.");
          return;
     }
     if (num < 1) {
          alert("There must be at least one number of evens.");
          return;
     }

     //for loop
     var result = "Starting with " + start + ", the next " + num + " even numbers are";
     var count = 0;
     var i = start;
     while (count < num) {
          if (i % 2 == 0) {
               count++;  
               if (count == num) {
                    result += " and " + i + ".";
               } else {
                    result += " " + i + ",";
               }
          } 
          i++;
     } 
     document.getElementById("result").innerText = result;
}
