var score, bestScore, roll, bestRoll; //game tracking

//Plays one round
function playGame(numDice, target, winAmt, loseAmt){
     //sum specified number of six-sided dlice
     var total = 0;
     for (var i = 0; i < numDice; i++) {
          total+=Math.ceil(Math.random()*6);
     }
     //calculate score and track best
     score += (total == target) ? winAmt : loseAmt;
     roll++;
     if (score > bestScore){ 
          bestScore = score;
          bestRoll = roll;
     }
     console.log("Roll: " + total + ", Score: " + score.toFixed(2));
}

//Plays as many rounds as possible
function playSeries(startScore, numDice = 2, target = 7, winAmt = 4, loseAmt = -1){
     //check for valid input
     startScore = startScore.replace(/^\$/, ''); //ignore initial "$" symbol
     if (isNaN(startScore) | startScore <= 0) {
          alert("Please enter a number greater than zero.");
          return;
     }
     //initialize variables and play game until bust
     score = bestScore = startScore = parseFloat(startScore); 
     roll = bestRoll = 0;
     console.log("***NEW GAME***\nStarting Score: " + startScore.toFixed(2));
     while (score > 0) {
          playGame(numDice, target, winAmt, loseAmt);
     }
     //display results and reset game
     document.getElementById("summary").style.display = "block";
     document.getElementById("start").textContent = "$" + startScore.toFixed(2);
     document.getElementById("rolls").textContent = roll;
     document.getElementById("max").textContent = "$" + bestScore.toFixed(2);
     document.getElementById("rollsForMax").textContent = bestRoll;
     document.getElementById("bet").value = "$0.00";
     document.getElementsByTagName("button")[0].textContent = "Play Again";
}
