function display(){
     //check input validity
     start = document.getElementById("start").value.trim();
     end = document.getElementById("end").value.trim();    
     step = document.getElementById("step").value.trim();
     if (start.length == 0 || end.length == 0 || step.length == 0 || isNaN(start) || isNaN(end) || isNaN(step)) {
          alert("Please enter three numbers.");
          return;
     }
     start = parseFloat(start);
     end = parseFloat(end);
     step = parseFloat(step);
     if (end <= start) {
          alert("Ending number must be greater than the starting number.");
          return;
     }    
     if (step < 1) {
          alert("Step must be a positive number.");
          return;
     }
     console.log(start + ", " + end + ", " + step);
     var output = "Here are the even numbers between " + start + " and " + end + " by " + step + "'s:";
     //loop from start to end by step
     for (i = start; i < end; i+= step){
          console.log(i);
          if (i % 2 == 0) {
               output+="<div>"+i+"</div>";     
          }
     }
     document.getElementById("result").style.display = "block";
     document.getElementById("output").innerHTML = output;
}

