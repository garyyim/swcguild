﻿using System;

namespace Warmups.BLL
{
    public class Strings
    {

        public string SayHi(string name)
        {
            return "Hello " + name + "!";
        }

        public string Abba(string a, string b)
        {
            return a + b + b + a;
        }

        public string MakeTags(string tag, string content)
        {
            return $"<{tag}>{content}</{tag}>";
        }

        public string InsertWord(string container, string word)
        {
            int mid = container.Length/2;
            return container.Substring(0, mid) + word + container.Substring(mid);
        }

        public string MultipleEndings(string str)
        {
            string lastTwo = str.Substring(str.Length-2);
            return lastTwo + lastTwo + lastTwo;
        }

        public string FirstHalf(string str)
        {
            throw new NotImplementedException();
        }

        public string TrimOne(string str)
        {
            throw new NotImplementedException();
        }

        public string LongInMiddle(string a, string b)
        {
            throw new NotImplementedException();
        }

        public string RotateLeft2(string str)
        {
            throw new NotImplementedException();
        }

        public string RotateRight2(string str)
        {
            throw new NotImplementedException();
        }

        public string TakeOne(string str, bool fromFront)
        {
            throw new NotImplementedException();
        }

        public string MiddleTwo(string str)
        {
            throw new NotImplementedException();
        }

        public bool EndsWithLy(string str)
        {
            throw new NotImplementedException();
        }

        public string FrontAndBack(string str, int n)
        {
            throw new NotImplementedException();
        }

        public string TakeTwoFromPosition(string str, int n)
        {
            throw new NotImplementedException();
        }

        public bool HasBad(string str)
        {
            throw new NotImplementedException();
        }

        public string AtFirst(string str)
        {
            throw new NotImplementedException();
        }

        public string LastChars(string a, string b)
        {
            throw new NotImplementedException();
        }

        public string ConCat(string a, string b)
        {
            throw new NotImplementedException();
        }

        public string SwapLast(string str)
        {
            throw new NotImplementedException();
        }

        public bool FrontAgain(string str)
        {
            throw new NotImplementedException();
        }

        public string MinCat(string a, string b)
        {
            throw new NotImplementedException();
        }

        public string TweakFront(string str)
        {
            throw new NotImplementedException();
        }

        public string StripX(string str)
        {
            throw new NotImplementedException();
        }
    }
}
