﻿using System; 

namespace Warmups.BLL
{
    public class Conditionals
    {
        public bool AreWeInTrouble(bool aSmile, bool bSmile)
        {
            return aSmile == bSmile; 
        }

        public bool CanSleepIn(bool isWeekday, bool isVacation)
        {
            return !isWeekday || isVacation;
        }

        public int SumDouble(int a, int b)
        {
            return (a == b) ? (a + b) * 2 : a + b; 
        }
        
        public int Diff21(int n)
        {
            if (n > 21) return Math.Abs(21 - n) *2;
            return Math.Abs(21 - n); 
        }
        
        public bool ParrotTrouble(bool isTalking, int hour)
        {
            return isTalking && (hour < 7 || hour > 20);
        }
        
        public bool Makes10(int a, int b)
        {
            return a == 10 || b == 10 || a + b == 10;
        }
        
        public bool NearHundred(int n)
        {
            return Math.Abs(100 - n) <= 10;
        }
        
        public bool PosNeg(int a, int b, bool negative)
        {
            if (negative) return a < 0 && b < 0;
            return a * b < 0;
        }
        
        public string NotString(string s)
        {
            if (s.Length >= 3 && s.Substring(0, 3) == "not") return s;
            else return "not " + s;
        }
        
        public string MissingChar(string str, int n)
        {
            return str.Substring(0, n) + str.Substring(n + 1);
        }
        
        public string FrontBack(string str)
        {
            if (str.Length <= 1) return str;
            return str.Substring(str.Length-1) + str.Substring(1, str.Length - 2) + str.Substring(0, 1);
        }
        
        public string Front3(string str)
        {
            string front = str.Length >= 3 ? str.Substring(0, 3) : str;
            return front + front + front;
        }
        
        public string BackAround(string str)
        {
            string last = str.Substring(str.Length - 1);
            return last + str + last;
        }
        
        public bool Multiple3or5(int number)
        {
            return (number % 3 == 0 || number % 5 == 0);
        }
        
        public bool StartHi(string str)
        {
            return str.Split(' ',',')[0].ToLower() == "hi";
        }
        
        public bool IcyHot(int temp1, int temp2)
        {
            return (temp1 < 0 && temp2 > 100) || (temp1 > 100 && temp2 < 0);
        }
        
        public bool Between10and20(int a, int b)
        {
            return (a >= 10 && a <= 20) || (b >= 10 && b <= 20);
        }
        
        public bool HasTeen(int a, int b, int c)
        {
            return (a >= 13 && a <= 19) ||
                   (b >= 13 && b <= 19) ||
                   (c >= 13 && c <= 19);
        }
        
        public bool SoAlone(int a, int b)
        {
            bool aIsTeen = (a >= 13 && a <= 19);
            bool bIsTeen = (b >= 13 && b <= 19);
            return aIsTeen ^ bIsTeen;
        }
        
        public string RemoveDel(string str)
        {
            if (str.Length >= 4 && str.Substring(1, 3) == "del")
                return str.Substring(0, 1) + str.Substring(4);
            else
                return str;
        }
        
        public bool IxStart(string str)
        {
            throw new NotImplementedException();
        }
        
        public string StartOz(string str)
        {
            throw new NotImplementedException();
        }
        
        public int Max(int a, int b, int c)
        {
            throw new NotImplementedException();
        }
        
        public int Closer(int a, int b)
        {
            throw new NotImplementedException();
        }
        
        public bool GotE(string str)
        {
            throw new NotImplementedException();
        }
        
        public string EndUp(string str)
        {
            throw new NotImplementedException();
        }
        
        public string EveryNth(string str, int n)
        {
            throw new NotImplementedException();
        }
    }
}
