﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hello_World
{
    class Program
    {
        static void Main(string[] args)
        {
            var x = GetOddNumber();
            var y = x++;
            var z = ++x;
            Console.WriteLine(x);
            Console.WriteLine(y);
            Console.WriteLine(z);
            Console.WriteLine("Hello world!");
            Console.WriteLine(z = 5000);

            Console.ReadLine();
        }

        static int GetOddNumber()
        {
            string input;
            int output;

            while (true)
            {
                Console.Write("Enter an odd number: ");
                input = Console.ReadLine();

                if (int.TryParse(input, out output))
                {
                    if (output % 2 == 0)
                    {
                        Console.WriteLine("That's not odd! Try again.");
                        continue;
                    }
                    else
                        break;
                }
            }
                return output;
        }
    }
}